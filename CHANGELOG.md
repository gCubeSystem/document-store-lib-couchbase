This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Document Store Backend Connector Library for CouchBase


## [v2.0.0-SNAPSHOT] [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19115]
- Enhanced couchbase java client version from 2.2.7 to 2.7.11. The selected version is the highest available which is compatible with the current server version. https://docs.couchbase.com/java-sdk/current/compatibility-versions-features.html


## [v1.6.0] [r4.12.1]- 2018-10-10

- Created uber-jar instead of jar-with-dependencies [#10150]


## [v1.5.0] [r4.7.0] - 2017-10-09

- Discover bucket in relation of dinamically discovered buckets [#9612]
- Implemented isConnectionActive() in PersistenceBackend provided implementation
- Used DSMapper to get Record from JsonNode


## [v1.4.0] [r4.5.0] - 2017-06-07

- Fixed bug on shutdown() method to properly close the connection


## [v1.3.1] [r4.4.0] - 2017-05-02

- Added shutdown() method to correctly close connections to persistence [#7345]


## [v1.3.0] [r4.3.0] - 2017-03-16

- Optimized number of connections to couchbasew server [#6305]
- Implemented the new API to close connection (if any) to persistence introduced by document-store-lib v1.5.0


## [v1.2.0] [r4.1.0] - 2016-11-07

- Modified cluster connection timing


## [v1.1.0] [r4.0.0] - 2016-06-29

- Different Usage Records are saved in different buckets [#4482]


## [v1.0.1] [r3.11.0] - 2016-04-08

- Fixed distro directory


## [v1.0.0] [r3.10.0] - 2016-02-08

- First Release

